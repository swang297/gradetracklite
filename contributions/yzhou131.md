# Contributions

This document states the contributions by Yetong Zhou (@yzhou131 - 260929206).

## Completed Tasks

I did the following tasks for the project.

- [Frontend Dashboard](#frontend-dashboard)
- [Frontend-Backend Coordination](#frontend-backend-coordination)
- [General Coordination](#general-coordination)
- [Architecture](#architecture-deliverable)

## Frontend Dashboard

I was responsible for implementing the dashboard (the page after login) for the frontend. As of the document deadline, it should be the files ending in .jsx as we were preparing for a potential migration to Vite.

## Frontend-Backend Coordination

I was responsible for making sure the frontend and backend worked together in terms of what API requests existed, their routes, how the request and responses should be formatted, etc. throughout the project. This included making a word document containing all the API requests and their expected requests and responses, which @zhu22 eventually adapted and codified into the server's [README.md](../server/README.md).

This task culminated in me leading a large refactoring of the backend and frontend (!1) that reconciled the differences, and added middlewares to the backend to reduce code duplication.

## General Coordination

I took part in coordinating the project on Discord, and other tasks such as managing the project files, creating a couple of diagrams, etc.

## Architecture deliverable

Since I code for both the frontend and backend, most of my writings for the deliverables were focussed on [Architecture.md](../Architecture.md).
