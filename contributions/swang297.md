# Contributions

This document states the contributions by Sean Wang (@swang297 - 260931012).

## Assigned Tasks

I was assigned the following tasks for the project:

- [Organization](#organization)
- [Front-End](#front-end)
- [Back-End](#back-end)
- [Deliverables](#deliverables)

## Organization

I organized meetings, set dates and times for people to agree on and ensured that people showed up through Direct Messaging on Discord.
I kept people updated on deadlines and kept everyone on track for the next optimal checkpoints in our development process.

## Front-End

I implemented the UI for the Login and Registration pages using React. I collaborated actively with @zhu22 in the back-end process of these two pages.

## Back-End

For the back-end, I was requested to help with the "edit" API requests along with @zhu22 and @yzhou131.

# Deliverables

I wrote up the Architectural Description and Report deliverables along with @zhu22 and @yzhou131.
I helped write Requirements.md, specifically for the Privacy by Design part.
