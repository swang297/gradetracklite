# Contributions

This document states the contributions by Malcolm Sutcliffe (@msutcl1 - 260853138).

## Assigned Tasks

I was assigned the following tasks for the project:

- [Deliverables](#deliverables)

## Deliverables

I wrote the [Requirements](https://gitlab.cs.mcgill.ca/yzhou131/comp555-project-team7/-/blob/dcocs/documentation/Requirements.md) deliverable and helped with the [Important Scenarios Section]() of the Architecture deliverable.
